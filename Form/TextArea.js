import React from 'react'
import {Field,ErrorMessage} from "formik"
import TextError from "./TextError"

function TextArea(props) {
    const {name} = props;
    return (
        <>
      <Field as ="textarea" name = {name} placeholder = {name} />
      <ErrorMessage name = {name} component = {TextError}/>
      </>
    )
}

export default TextArea
