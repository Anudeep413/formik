import React from 'react'
import {Field,ErrorMessage} from "formik"
import TextError from "./TextError"
import { TextField ,Container,MenuItem} from '@material-ui/core';
import Select from '@material-ui/core/Select'
function Selects(props) {
    const {name,dropdown,label,width} = props
  
    console.log(props)
    return (
        <div>
            <Field  as={TextField} select  name = {name} fullWidth label = {label}  >
            {
                dropdown.map((item) => {
                    return <MenuItem key = {item.key} value = {item.value} > {item.key} </MenuItem>
                })
            }
            </Field>
            <ErrorMessage name ={name} component={TextError} />

        </div>
    )
}



export default Selects
