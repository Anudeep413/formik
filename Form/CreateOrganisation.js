import React from 'react'
import { Container, Grid, Paper, Typography, TextField, Button } from "@material-ui/core"
import { Field, Form, Formik, ErrorMessage } from "formik"
import * as Yup from "yup"
import FormikControl from "./FormikControl"

function CreateOrganisation() {
    const arr = new Array(1);

    console.log(arr.length)

  const  initialValues = {
        OrganisationName1 : "",
        work : ""
    }

    const dropdown = [
        { key: "select an option", value: "" },
        { key: "Work1", value: "Work1" },
        { key: "Work 2 ", value: "Work2" },
        { key: "Work 3 ", value: "Work3" },
    ]

    const validationSchema = Yup.object({

    })

    const onSubmit = (values) => {

    }

    return (
       <Formik initialValues={initialValues} onSubmit = {onSubmit}   enableReinitialize >
           {(formik) => {
             return <> <Container>
                
               <Form>
               <br></br>
                   
                   <Typography variant = "h3" >Create Organisation</Typography>
                   <br></br>
                   <br></br>
                   <Grid container mt={3} spacing={6} xs={12} >
                                <Grid item xs={12} sm={6} >
                                <Typography variant="small">Organisation Name</Typography>
                                    <FormikControl control="input" name="OrganisationName1"  label="Eg. Tata Industries" />
                                
                                </Grid>
                                <Grid item  xs={12} sm={6} >
                                <Typography variant="small">Choose Work</Typography>
                                    <FormikControl control="select" name="work" label="Rubber Factory" dropdown = {dropdown} />
                                
                                </Grid>
                                <Grid item sm={6}>
                                    <Button onClick = { () => {arr.push("")}  }  color="primary" >
                                        Add Another 
                                        </Button>              
                                </Grid>

                                </Grid>

               </Form>
               </Container>
               </>
           }}
       </Formik>
    )
}

export default CreateOrganisation
