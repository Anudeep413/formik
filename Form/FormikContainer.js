import React from 'react'
import * as Yup from "yup"
import {Formik,Form,Field, validateYupSchema, ErrorMessage} from "formik"
import FormikControl from "./FormikControl"

function FormikContainer() {

    const dropdown = [
        { key : "select an option", value : ""},
        {key : "Option 1 ", value : "java"  },
        {key : "Option 2 ", value : "python"  },
        {key : "Option 3 ", value : "c++"  },
    ]

    const initialValues = {
        name : "",
        description : "",
        selectOption : ""
    }

    const validationSchema = Yup.object({
        name : Yup.string().required("Required"),
        description : Yup.string().required("Required"),
        selectOption : Yup.string().required("Required")
    })

    const onSubmit = (values) => {
            console.log("values",values)
    }
    return (
        <Formik initialValues = {initialValues} validationSchema = {validationSchema} onSubmit = {onSubmit}  >
            {formik => {
                console.log(formik)
                return <>
                <Form>
                    <FormikControl name = "name" control = "input" />
                    <br></br>
                    <FormikControl name = "description" control = "textarea" />
                    <br></br>
                    
                    <FormikControl name = "selectOption" control = "select" dropdown = {dropdown} />
                    <button type = "submit" >submit</button>
                </Form>
                </>
            }}
        </Formik>
    )
}

export default FormikContainer

{/* <Field as="select" name = "selectOption"  >
                        {
                            dropdown.map( item => {
                                return <option key ={item.key} value  ={item.value}  > {item.key} </option>
                            }  )
                        }
                        </Field>
                        <ErrorMessage name = "selectOption"/> */}