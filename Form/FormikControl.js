import React from 'react'
import Input from "./Input"
import TextArea from "./TextArea"
import Selects from "./Select"

function FormikControl({name,control,...rest}) {
    // console.log(props)
    // const {name,control} = props;
    //  console.log(name,control)
    //  console.log(props)
    //  const {name,control} = props

     console.log(name,control);


    switch(control){
        case "input" : 
        return <Input name = {name} {...rest}  />
        case "textarea" : 
        return <TextArea name = {name}  />
        case "select" : 
        return <Selects name = {name} {...rest} />
        default : 
        return null
    }
   
}

export default FormikControl
