import React from 'react'
import {Field,ErrorMessage} from "formik"
import TextError from "./TextError"
import { TextField ,Container} from '@material-ui/core';

function Input(props) {
    const {name,label} = props;
    console.log("Calledd in inputtt")
    return (
        <>
       
      <Field as={TextField} name = {name} fullWidth label = {label} >
      </Field>
      <ErrorMessage name = {name} component = {TextError}/>
     
      
      </>
    )
}

export default Input
