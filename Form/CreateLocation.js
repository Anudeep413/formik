import React from 'react'
import { Container, Grid, Paper, Typography, TextField, Button } from "@material-ui/core"
import { Field, Form, Formik, ErrorMessage } from "formik"
import InputLabel from '@material-ui/core/InputLabel';
import * as Yup from "yup"
import FormikControl from "./FormikControl"


function CreateLocation() {

    const dropdown = [
        { key: "select an option", value: "" },
        { key: "Option 1 ", value: "java" },
        { key: "Option 2 ", value: "python" },
        { key: "Option 3 ", value: "c++" },
    ]

    const initialValues = {
        selectOrganisation: "",
        locationName: "",
        selectOption: "",
        country: "",
        building: "",
        street: "",
        city: "",
        state: "",
        pinCode: "",
        floors: ""
    }

    const validationSchema = Yup.object({
        selectOrganisation: Yup.string().required("Required"),
        locationName: Yup.string().required("Required"),
      
        country: Yup.string().required("Required"),
        building: Yup.string().required("Required"),
        street: Yup.string().required("Required"),
        city: Yup.string().required("Required"),
        state: Yup.string().required("Required"),
        pinCode: Yup.string().required("Required"),
        floors: Yup.string().required("Required"),

    })

    const onSubmit = (values) => {
        console.log("values", values)
    }
    return (
        <Formik  initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}  >

            { (formik) => {
                console.log(formik)
                return (
                    <Form>

                        <Container>
                            <br></br>
                            <br></br>
                            <Typography variant="h5">Create Location</Typography>

                            <br></br>
                            <Grid container mt={3} spacing={3} xs={12} >
                                <Grid item xs={12} sm={6} >
                                    <br></br>
                                    <FormikControl name="selectOrganisation" placeholder="Name" control="select" label="Select Organisations"
                                        dropdown={[{ key: "qwd", value: "qdwwqd" }, { key: "qwd", value: "qdwwqd" }]} />
                                    <br></br>
                                    <br></br>
                                    <Button variant="contained" color="green" fullWidth>Detect Location</Button>
                                </Grid>
                                <Grid item xs={12} sm={6} >
                                    <Typography variant="small">Location Name</Typography>
                                    <FormikControl control="input" name="locationName" label="Rubber Factory" />
                                </Grid>

                                <FormikControl control="input" name="country" label="Country" />
                                <FormikControl control="input" name="building" label="Building no/House no." />
                                <FormikControl control="input" name="street" label="Street/Area" />
                                <Grid container mt={3} spacing={3} xs={12} >
                                    <Grid item xs={12} sm={6}>
                                        <FormikControl control="input" name="city" label="City" />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <FormikControl control="input" name="state" label="State" />
                                    </Grid>
                                </Grid>
                                <FormikControl control="input" name="pinCode" label="Pin Code" />
                                <Grid container mt={3} spacing={3} xs={12} >
                                    <Grid item xs={12} sm={6}>
                                        <FormikControl control="input" name="floors" label="No Of Floors" />
                                    </Grid>
                                </Grid>
                                <Grid item sm={6}>
                                    <Button color="primary" fullWidth>
                                        Add Another Location
                                        </Button>              
                                </Grid>

                                <Grid container mt={3} spacing={3} xs={12} >
                                    <Grid item xs={12} sm={6} >
                                        <Button variant="contained" color="primary" fullWidth>
                                            Previous
                                            </Button>
                                    </Grid>
                                    <Grid item xs={12} sm={6} >
                                        <Button variant="contained" color="primary" fullWidth>
                                            Create
                                </Button>
                                    </Grid>
                                </Grid>


                            </Grid>
                            <br></br>
                            <br></br>

                           
                        </Container>
                        <button disabled={!formik.isValid} type = "submit" >wefwef</button>
                    </Form>)
                
            }}


        </Formik>
    )
}

export default CreateLocation

{/* <Grid  container mt={3}  spacing={3} xs = {12} >
<Grid  item xs={12} sm={6} >

<TextField
id="standard-select-currency"
select
label="Select Organisation"
fullWidth
/>
<br></br>
<br></br>
<br></br>
<br></br>

<Button  variant="contained" color="green" fullWidth>
Detect Location
</Button>
</Grid> */}